<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-sm navbar-light index">
            <a class="navbar-brand" href="#">
                <img src="img/logo.png" alt="">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="form-inline my-2 my-lg-0 offset-md-9">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                Swedish</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">English</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Español</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact.php">
                                contacto</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>