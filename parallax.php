 <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="parallax-image2">
            <div class="titulos">
                <span >Museo Vasa</span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="parallax-description">

            <p> Situado en la isla de Djurgarden, donde se concentran también el museo al aire libre de Skanseno el parque de
                atracciones Grona Lund, la fisonomía exterior del edificio del museo Vasa ya te anticipa que algo no habitual
                vas a ver en su interior. El Vasa es el único barco del siglo XVII que ha sobrevivido hasta nuestros días.
                Con más del 98 % de su estructura original y sus cientos de esculturas talladas, el Vasa es un tesoro artístico
                y uno de los monumentos turísticos más visitados del mundo.
            </p>

        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="parallax-image3">
            <div class="titulos">
                <span> Palacio Drottningholm</span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="parallax-description">
            <p>Ubicado en la isla Lovön, el Palacio Drottningholm (o “Islote de la Reina”), ha sido el hogar de la familia real
                de Suecia desde el año 1981. El palacio original fue construido en el siglo XVI a instancias del rey Johan
                III, para su esposa, la reina Katarina Jagellonika. Este primer edificio fue destruido por un incendio en
                1661. En 1662 comenzó la construcción del nuevo palacio, por orden la reina Dowager. Este es el palacio que
                se conserva hasta la fecha.
            </p>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="parallax-image1">
            <div class="titulos">
                <span>Paseo en Barco </span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="parallax-description">

            <p>Situada sobre 14 islas principales, Estocolmo cuenta con bahías, canales, playas, puentes y rutas en barco para
                explorar la ciudad con visitas guiadas desde el agua.</p>
        </div>
    </div>
</div>